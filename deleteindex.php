<?php

require 'IndexerCurls.php';

echo "\n" . '** Index name to delete :';
$index = trim(fgets(STDIN));

echo "\n" . '** index ' . $index . ' will be deleted !!!!, procced ? ' . "\n" . 'y/n ? : ';
ask_confirmation( $index );

function ask_confirmation( $index ) {
	$answer = trim(strtolower(fgets(STDIN)));
	delete_confirmation( $answer, $index );
}

function delete_confirmation( $answer, $index, $check_again = null ) {

	if ($answer == 'y') {

		doubleCheck( $answer, $index );		
	}

	if ($answer == 'n') {

		terminate();
	}

	if ($answer != 'y' || $answer != 'n') {
	
		reEnter( $answer, $index );
	}
		
}

function doubleCheck ( $answer, $index )
{
	echo '** are you sure ? ';
		$check_again = trim(fgets(STDIN));
		
		if ($check_again == 'y') {
			deleteIndex($index);
			echo "** index '" . $index . "' deleted **" . "\n";	
			die;

	 	} 
}

function reEnter( $answer, $index )
{
	echo "! please enter 'y' or 'n' !";
	ask_confirmation( $index );
	
}

function terminate()
{
	echo '** terminating... **' . "\n";
	die;
}
