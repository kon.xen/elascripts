<?php

/**
 * summary: creates and bulk populates an index
 * @var $file_names_to_index
 * @var $new_index
 **/
function createIndex( $file_names_to_index, $new_index ) 
{
	for ($i = 1; $i <= count($file_names_to_index); $i++) {

		if (!is_file($file_names_to_index[$i])) {
			echo "\n".'! error :  File not found '."\n";
			return;
		}

		echo "\n".'== now indexing '.$file_names_to_index[$i]."=="."\n";

		$request = curl_init();

		$url     = 'http://localhost:9200/'.$new_index.'/_bulk?pretty&refresh';
		$options = [
			CURLOPT_URL            => $url,
			CURLOPT_POST           => 1,
			CURLOPT_POSTFIELDS     => file_get_contents($file_names_to_index[$i]),
			CURLOPT_HTTPHEADER     => ['Content-Type: application/x-ndjson'],
			CURLOPT_RETURNTRANSFER => 1
		];

		curl_setopt_array($request, $options);

		$result = curl_exec($request);

		if (curl_errno($request)) {
			echo 'Error:'.curl_error($request);
		}

		$create_output = curl_getinfo($request);
		
		echo "\n".'====== Done! ======'."\n";
		echo "\n".'=== indexed '.$create_output['size_upload'].' bytes '.'in: '.$create_output['total_time']%60 .' seconds ==='."\n";

		curl_reset($request);

		curl_close($request);
	}
}


/**
 * summary: deletes an index
 * @var $index
 **/
function deleteIndex( $index )
{
	$request = curl_init();

	$url     = 'localhost:9200/'. $index . '?pretty';
	$options = [
		CURLOPT_URL            => $url,
		CURLOPT_CUSTOMREQUEST  => 'DELETE',
		CURLOPT_RETURNTRANSFER => 1
	];

	curl_setopt_array($request, $options);

	$result = curl_exec($request);

	if (curl_errno($request)) {
		echo 'Error:' . curl_error($request);
	}

	//print_r(curl_getinfo($request));//testing

	curl_close($request);
}
