# With this script you can bulk index .json files in to elastic search.
### this script is build to poerate within a specific context, alteration will be needed to suit your needs.

***Requirements:***
* In order to use PHP's cURL functions you need to install the » libcurl package. PHP requires libcurl version 7.10.5 or later.
* CURL enabled in PHP
* a .json file with no more than 5 fields ! 
	
Ubuntu users:
```sudo apt install php-curl``` 
```sudo apt install libcurl4```

Windows users: 
change to Linux!

	

***usage:***

To modify the json file and perform bulk index.
```php index.php```

To only bulk index a .json file.
```php loadindex.php```

To delete an index.
```php deleteindex.php```


**NOTE** 

**you will need to edit the *$url* in the *Loader.php* to corespond to your elasticsearch url**

Have fun :)