<?php

require 'Modifier.php';
require 'Loader.php';

echo "\n".'File to index : ';
$file = trim(fgets(STDIN));

echo "\n".'Index name :';
$new_index = trim(fgets(STDIN));

echo "\n".'any existing files will be overwriten, procced ? y / n : ';
ask_confirmation();

$that_many_files = evaluateSize($file);

modifyFile($file, $that_many_files);
createIndex($file_names_to_index, $new_index);

function ask_confirmation() {
	$answer = trim(strtolower(fgets(STDIN)));
	check_confirnation($answer);
}

function check_confirnation($answer) {
	if ($answer == 'y') {
		return;
	}

	if ($answer == 'n') {
		echo 'terminating...';
		die;
	}

	if ($answer != 'y' || $answer != 'n') {
		echo 'please enter y or n ';
		ask_confirmation();
	}
}
