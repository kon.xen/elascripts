<?php

/**
 * summary: Main Function. modifies the json file so can be imported in elasticSearch
 * @var $file
 **/
function modifyFile( $file, $that_many_files ) 
{
	$starting_time = gettimeofday();

	$file_contents = file($file, FILE_IGNORE_NEW_LINES);
	$lines         = count($file_contents);

	$file_names_to_index = [];
	global $file_names_to_index;

	if ($that_many_files >= 2) {

		$file_chunks  = calculateChunks($file, $lines);
		$no_of_chunks = count($file_chunks);

		for ($i = 1; $i <= $no_of_chunks; $i++) {

			$new_filename = newFilename($file, $i);
			//stristr( $file, '.', true ) . "_new". $i . ".json";
			$file_names_to_index[$i] = $new_filename;
			$line_counter            = 0;
			checkFileExists($new_filename);

			$writeto = fopen($new_filename, "x");

			for ($y = $file_chunks[$i]['start']; $y <= $file_chunks[$i]['end']; $y++) {

				writeAllThatJazz($writeto, $file_contents, $y);

				$line_counter++;
			}

			fclose($writeto);
			output($starting_time, $line_counter, $new_filename);
		}

	} else {

		$new_filename = newFilename($file, $increment = null);
		$file_names_to_index[1] = $new_filename;
		$line_counter           = 0;
		checkFileExists($new_filename);

		$writeto = fopen($new_filename, "x");

		for ($y = 0; $y <= $lines-1; $y++) {

			writeAllThatJazz($writeto, $file_contents, $y);

			$line_counter++;
		}

		fclose($writeto);
		output($starting_time, $line_counter, $new_filename);
		return $new_filename;
	}
}

/**
 * @var $writeto
 * @var $file_contents
 * @var $y
 **/
function writeAllThatJazz( $writeto, $file_contents, $y )
{
	$brake_down = preg_split("/[{}:\s,]+/", $file_contents[$y]);
	$id         = $brake_down[2];
	$index      = '{'.'"index"'.':'.'{'.'"_id"'.':'.$id.'}}';
	$file_line  = trim($file_contents[$y], " ,[]");
	$line       = $index."\n".$file_line."\n";

	fwrite($writeto, $line);
}

/**
 * @var $file
 **/
function evaluateSize($file) {
	$file_size      = getSize($file);
	$fileSize_limit = 169000000;
	$no_of_files    = 1;

	if ($file_size > $fileSize_limit) {
		$no_of_files = 2;
		while (!isset($stop)) {
			$file_chunk = $file_size/$no_of_files;
			if ($file_chunk < $fileSize_limit) {
				$stop = true;
			} else {
				$no_of_files++;
			}
		}
	}

	return $no_of_files;
}


/**
 * @var $file
 **/
function getSize($file)
{
	return round(filesize($file), 0);
}

/**
 * @var $file
 * @var $i
 **/
function newFilename( $file, $increment = null )
{
	return (stristr($file, '.', true)."_new".$increment.".json");
}

/**
 * @var $new_filename
 **/
function checkFileExists( $new_filename )
{
	if (is_file($new_filename)) {
		unlink($new_filename);
	}
}

/**
 * @var $starting_time
 **/
function calculateTime( $starting_time )
{
	$ending_time = gettimeofday();
	$time        = [];

	$elapsed_time = $ending_time['sec']-$starting_time['sec'];

	$hour = $elapsed_time/3600%24;
	$mins = $elapsed_time/60%60;
	$secs = $elapsed_time%60;

	return $time = ['h' => $hour, 'm' => $mins, 's' => $secs];
}

/**
 * @var $file
 * @var $lines
 **/
function calculateChunks( $file, $lines )
{
	$that_many_files = evaluateSize($file);
	$chunk_of_lines  = round($lines/$that_many_files, 0);
	$line_chunks     = [];

	$line_chunks[1] =
	[
		'start' => 0,
		'end'   => $chunk_of_lines
	];

	$start_of_last_chunk = $line_chunks[1]['end']+1;

	if ($that_many_files >= 2) {
		for ($i = 2; $i <= $that_many_files-1; $i++) {

			$previous = $i-1;

			$line_chunks[$i] =
			[
				'start' => $line_chunks[$previous]['end']+1,
				'end'   => $line_chunks[$previous]['end']+$chunk_of_lines+1
			];

			$start_of_last_chunk = $line_chunks[$previous]['end']+$chunk_of_lines;
		}
	}

	$line_chunks[$that_many_files] =
	[
		'start' => $start_of_last_chunk,
		'end'   => $lines-1
	];

	return $line_chunks;
}

/**
 * @var $new_filename
 * @var $lines
 * @var $starting_time
 **/
function output( $starting_time, $lines, $new_filename ) 
{
	$overal_time = calculateTime($starting_time);

	echo "\n".'=== added: '.$lines.' index lines'.' to '.$new_filename.' in '.$overal_time['h'].':'.$overal_time['m'].':'.$overal_time['s'].' ==='."\n";
}
